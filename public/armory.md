Datacenter Epsilon: Armory
==========================

- Impact Driver
  + Uses kinetic energy to damage enemies at close range.
- Point Gun
  + Fires point-energy that immediately harms enemies in line-of-sight.
- Spread Gun
  + Fires spread-energy that immediately harms multiple enemies in a
  small area directly ahead.
- Chain Gun
  + Fires point-energy in a rapid pattern.
- Bazooka
  + Launches small self-propelled ballistic missiles with warheads that
  detonate on impact.
- Ion Cannon
  + The ion bolts it produces look like balls of blue lightning and
  burst with electrical power on impact.
- Photon Cannon
  + The photonic energy ball it projects is red and produces a large
  red flash of light on impact.
- Vibro Blade
  + Uses supersonic vibrations to severely damage enemies at close range.
- Scatter Gun
  + Fires spread-energy that immediately harms multiple enemies in a
  wide area directly ahead.
