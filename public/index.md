Project Exosuit
===============

UNDER CONSTRUCTION


Beyond Milestone 1
------------------

The base WAD is a drop-in UI/HUD replacement. The space ranger can 
infiltrate the demoic corridors of Hell or assault the techbases of 
Freedoom. Supplemental WADs might offer new enemies, new maps, and new 
stories.

### Datacenter Epsilon: Galactic Encyclopedia

A collection of logbook info detailing some lore behind Freedoom's 
monsters and more.

### Assault on Mercenary Outpost DHE-31

A three- or five-map WAD about infiltrating and destroying an enemy 
base. It will have a structure similar to Chex Quest: surface, storage, 
caverns.


External Links
--------------

- [Chocolate Doom](https://www.chocolate-doom.org/)
- [Freedoom](https://freedoom.github.io/)
- [OpenGameArt](https://opengameart.org/)
- [PrBoom+](https://prboom-plus.sourceforge.io/)
