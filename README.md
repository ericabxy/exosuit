![pixel outlined illustration of suit](images/suit-b.png)

## ABOUT EXOSUIT

Exosuit is a mod for Doom and other Boom-compatible games. Play as an 
elite space ranger operating a suit of powered armor that protects you 
in hostile environments and dangerous battlefields. Engage your 
enemies with an adaptable arsenal of weapons and equipment.


## HOW TO USE EXOSUIT

If you downloaded a release package, exosuit.wad should have been 
included. Load Exosuit as a PWAD using the -file flag.

    $ boom -file exosuit.wad

To play with a particular IWAD you will need to use the -iwad flag.

    $ boom -iwad freedoom2.wad -file exosuit.wad

To combine with another mod you'll need to add it to the -file flag, 
either before or after exosuit.wad depending on which assets you want to 
be overridden.

    $ boom -file kneedeep.wad exosuit.wad


## RECOMMENDED WADS

Certain Doom levelsets from the modding community work particularly well 
with Exosuit on an aesthetic level. The following WADs are favorites of 
the author.

  + https://www.doomworld.com/idgames/levels/doom2/s-u/sid
  + https://www.doomworld.com/idgames/levels/doom2/j-l/kneedeep
  + https://freedoom.github.io/


## BUILDING FROM SOURCE

If you downloaded a source package or made some of your own 
modifications to Exosuit, you'll have to compile the source assets into 
a new PWAD using DeuTex.

    $ deutex -make exosuit.wad

A dummy WAD named "doom2.wad" is included in the source tree. This is a 
minimal WAD with only the data required by Deutex when you invoke the 
above command (an empty textures list and a collection of color 
palettes).

## DESIGN GOALS

Replace the HUD, fonts, player sounds, player sprites, and pickup 
sprites to be more reminiscent of a series like Metroid or Megaman. The 
world of Doom itself remains intact, and the aesthetics of the mod do 
not stray from Doom very much: text stays red, guns stay centered, the 
behavior of weapons remain the same while changing in look and sound to 
be more techno. Notably any player sprites showing skin must be changed 
because the player is enclosed in an armored suit, and any sounds of the 
player's voice are better replaced by tones and alarms from the suit.


## CHANGES AND TODO

The weapon sprites PUNG, PISG, PLSG, SAWG, SHTG, and SHT2 normally show 
parts of the player's body and skin so they had to be changed.

The pickup sprites MEDI and STIM which restore the suit's power have 
been changed to resemble something out of Megaman or Metroid. They will 
likely be animated in the future to enhance the affect.

The AMMO, CELL, CELP, CLIP, RBOX, ROCK, SBOX, and SHEL pickups have been 
made to resemle their symbols in the status bar. They may all be 
modified to use brighter colors in the future.

Most of the "weapon ready" and firing sprites have been replaced with 
placeholders or something resembling their intended lore, which can be 
found in "docs/concepts.txt".

Weapon sounds are in the process of being replaced in order to have 
consistency instead of changing depending on the IWAD you're using.

The player sprite PLAY normally has exposed skin. Work is ongoing to 
design a replacement that looks more like powered armor, but it doesn't 
affect the single-player experience for now.

The pickups BON1, BON2, MEGA, PINS, PINV, PMAP, PSTR, PVIS, SOUL, BKEY, 
BSKU, RKEY, RSKU, YKEY, and YSKU are considered enemy assets and 
nominally out-of-scope for this mod.

By means of a BEX patch, the player no longer "bleeds" when hit by enemy 
bullets.

## DESIGN ISSUES

The sound of the Point Gun (DSPISTOL) and Spread Gun (DSSHOTGN) is 
re-used by the engine for POSESSED, SHOTGUY, and CHAINGUY attack 
sounds. The mod cannot change this behavior, so any attempt to make 
these player weapons sound "more sci-fi" oversteps design goals, but 
the benefits may outweigh the drawbacks. This is one of the reasons 
the Spread Gun reload sound remains similar to a gun cocking instead 
of a futuristic beep.

The menu system re-uses the environmental sounds DSSWTCHN, DSPSTOP, 
DSPISTOL, and DSSWTCHX. This makes it difficult for the menu to sound 
"more sci-fi" since the sounds must be appropriate for gameplay *and* 
menu navigation.

The engine re-uses DSOOF for the sound of falling from a height *and* 
Using a door that is locked. Care must be taken to ensure the modified 
sound is appropriate for both actions. DSNOWAY is used only when Using 
a non-usable linedef and can be considered separately.

The CHAINGUY enemy drops a weapon that the player can use. Thus the 
CHAINGUY sprite, the weapon sprite that it drops, and the corresponding 
player weapon must all match in some way. This issue can be addressed by 
leaving these sprites alone completely, or making sure weapon 3 pickup 
and ready sprites are consistent with all known WADS, or turning the 
dropped weapon into a more "abstract" pickup.
